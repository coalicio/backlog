# Coalición 

[Avance de las Iniciativas]
---
## Iniciativas en curso
- Reducir reuniones de seguimiento personales.
- Herramienta colaborativa para reducir reuniones.
- Equipos conjuntos con proveedor.
- Potenciar trabajo en equipo.
- Sentimiento de grupo – Potenciarlo. 
- Innovació: Workshops / Formació 

---
## 26 - Reducir reuniones de seguimiento personales mediant nuevo modelo de documentación en arquitectura
 - Owner: ImmaV
 - Equipo: RaulS, LluisG

---
## Objetivo
Reducción de reuniones de seguimento personal, y del tiempo dedicado a preparar reuniones de seguimiento y responder consultas sobre estado de servicios/proyecto.

## Próximos pasos
1. Propuesta de esqueleto de documentación - Gerencia canales.
2. Piloto de 2 meses de duración. Fin: 09/03/2018.

---
## 34 - Potenciar herramienta colaborativa para reducir reuniones
 - Owner: JoseCarlosR
 - Equipo: AndresG, RaulM

---
## Objetivo
Reducir el uso del correo electrónico y sustituirlo por una herramienta de comunicación que permita mejorar el seguimiento de los proyectos 
y la coordinación intraequipos y con proveedores.

## Próximos pasos
1. Homologar cliente de mattermost para la plataforma.
2. Resolver problemáticas con app para android corporativo.

---
## 77 - Equipos conjuntos con proveedor
 - Resp. coalición: RaulS
 - Owner: MarcA 
 - Equipo:JRamonC, RobertS

---
## Objetivo
Definir un modelo básico de trabajo en proyectos y servicios que permita trabajar en equipo con el proveedor (en cotraposición a un modelo de
gestión cliente-proveedor) incluso en remoto.

## Próximos pasos
1. Recoger buenas prácticas + brainstorming en proyectos piloto. 

---
## 106 - Potenciar trabajo en equipo
 - Owner: AlbertM
 - Equipo: AlbertM, ImmaV

---
## Objetivo
Descentralizar la búsqueda de soluciones (jefe de proyecto - gerentes). Aumentar la conciencia de equipo dentro de la gerencia.

---
## Próximos pasos
1. Técnicas de trabajo en equipo - digitalización - sesión 3: 12/01/2018
2. Proyecto CaaS - sesión de estimación: 16/01/2018
3. Retrospectiva equipo digitalización (team assesment). 26/01/2018

---
## 176 Sentimiento de grupo – Potenciarlo
 - Owner: DanielJ
 - Equipo: MiquelP

---
## Objetivo
Mejorar la comunicación y la relación personal en arquitectura. 

## Próximos pasos
Medir el uso y aceptación de las diferentes propuestas:
* Kudos de agradecimiento en el departamento
* Ejercicio semanal para hacer con los compañeros
* Cambio de sitio

---
Obsequia un Kudo personalizado:
![Foto Kudos Arquitectura](/pictures/Kudos.jpg)

---
## 198 Innovació: Workshops i Formació
 - Equipo: RobertoS, JordiG

## Próximos pasos
* Brainstorming al que está invitado todo el departamento para definir la iniciativa: qué se hace, cómo y cuando.
* 12/02/2018 de 10:00 a 13:00.

